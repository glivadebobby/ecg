package in.glivade.ecg;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private Spinner spinnerPatients;
    private Button buttonReset, buttonPlot;
    private LineChart lineChart;
    private List<Patient> patientList;
    private List<EcgPlot> ecgPlotList;
    private ArrayAdapter<Patient> patientArrayAdapter;
    private ProgressDialog progressDialog;
    private int patientId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initObjects();
        initCallbacks();
        initSpinner();
        initChart();
        getPatients();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        patientId = patientList.get(position).getId();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        if (v == buttonReset) lineChart.clear();
        if (v == buttonPlot) {
            if (patientId >= 0)
                getPlots();
            else Toast.makeText(this, "Please select the patient", Toast.LENGTH_SHORT).show();
        }
    }

    private void initObjects() {
        spinnerPatients = findViewById(R.id.spin_patients);
        buttonReset = findViewById(R.id.btn_reset);
        buttonPlot = findViewById(R.id.btn_plot);
        lineChart = findViewById(R.id.chart);

        patientList = new ArrayList<>();
        ecgPlotList = new ArrayList<>();
        patientArrayAdapter = new ArrayAdapter<>(this, R.layout.item_spinner, patientList);
        progressDialog = new ProgressDialog(this);
    }

    private void initCallbacks() {
        spinnerPatients.setOnItemSelectedListener(this);
        buttonReset.setOnClickListener(this);
        buttonPlot.setOnClickListener(this);
    }

    private void initSpinner() {
        patientArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPatients.setAdapter(patientArrayAdapter);
    }

    private void initChart() {
        lineChart.setNoDataTextTypeface(TypefaceUtils.load(getAssets(), "fonts/ubuntu_regular.ttf"));
        lineChart.setNoDataTextColor(ContextCompat.getColor(this, R.color.primary_txt));
        lineChart.setNoDataText(getString(R.string.error_no_data));
    }

    private void getPatients() {
        showProgressDialog("Loading patients..");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://ecg.glivade.in/index.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        handlePatientResult(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                Toast.makeText(MainActivity.this,
                        error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("tag", "patients");
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, "patients");
    }

    private void handlePatientResult(String response) {
        patientList.clear();
        try {
            patientList.add(new Patient(-1, "Select Patient", null));
            JSONArray jsonArray = new JSONArray(response);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int id = jsonObject.getInt("indexId");
                String name = jsonObject.getString("patientName").trim();
                String date = jsonObject.getString("ecgDate").trim();
                patientList.add(new Patient(id, name, date));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        patientArrayAdapter.notifyDataSetChanged();
    }

    private void getPlots() {
        showProgressDialog("Loading plots..");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://ecg.glivade.in/index.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        handlePlotResult(response);
                        drawChart();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                Toast.makeText(MainActivity.this,
                        error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("tag", "plots");
                params.put("id", String.valueOf(patientId));
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, "plots");
    }

    private void handlePlotResult(String response) {
        lineChart.clear();
        ecgPlotList.clear();
        try {
            JSONArray jsonArray = new JSONArray(response);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String id = jsonObject.getString("id").trim();
                String content = jsonObject.getString("content").trim();
                ecgPlotList.add(new EcgPlot(id, content));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void drawChart() {
        ArrayList<Entry> entries = new ArrayList<>();
        ArrayList<Integer> values = new ArrayList<>();
        for (int i = 0; i < ecgPlotList.size(); i++) {
            EcgPlot ecgPlot = ecgPlotList.get(i);
            float val;
            try {
                val = Float.parseFloat(ecgPlot.getContent());
                values.add((int) val);
            } catch (NumberFormatException e) {
                val = 0;
            }
            entries.add(new Entry(i, val));
        }

        float flatLine = 0;
        float tempValue = 0;
        Set<Integer> uniqueSet = new HashSet<>(values);
        for (Integer value : uniqueSet) {
            if (value == 0) continue;
            int frequency = Collections.frequency(values, value);
            if (frequency > tempValue) {
                flatLine = value;
                tempValue = frequency;
            }
        }

        LineDataSet dataSet = new LineDataSet(entries, "ECG Data");
        dataSet.setValueFormatter(new LineDataValueFormatter(flatLine, entries));
        dataSet.setValueTextSize((float) (dataSet.getValueTextSize() * 1.2));
        dataSet.setColor(ContextCompat.getColor(this, R.color.primary));
        dataSet.setDrawCircles(false);

        LineData lineData = new LineData(dataSet);
        lineChart.setData(lineData);
        lineChart.getXAxis().setDrawGridLines(false);
        lineChart.getAxisLeft().setDrawGridLines(false);
        lineChart.getXAxis().setEnabled(false);
        lineChart.getAxisRight().setEnabled(false);
        lineChart.getDescription().setEnabled(false);
        lineChart.notifyDataSetChanged();
        lineChart.invalidate();
    }

    private void showProgressDialog(String message) {
        progressDialog.setMessage(message);
        progressDialog.setCanceledOnTouchOutside(false);
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
