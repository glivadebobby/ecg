package in.glivade.ecg;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.util.ArrayList;

public class LineDataValueFormatter implements IValueFormatter {

    private int lastType = 0;
    private float flatLine;
    private ArrayList<Entry> values;

    LineDataValueFormatter(float flatLine, ArrayList<Entry> values) {
        this.flatLine = flatLine;
        this.values = values;
    }

    @Override
    public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
        int nextIndex = (int) (entry.getX() + 1);
        switch (lastType) {
            case 0:
                if ((value > flatLine) && (value > values.get(nextIndex).getY())) {
                    lastType = 1;
                    return "P";
                }
                break;
            case 1:
                if ((value < flatLine) && (value < values.get(nextIndex).getY())) {
                    lastType = 2;
                    return "Q";
                }
                break;
            case 2:
                if ((value > flatLine) && (value > values.get(nextIndex).getY())) {
                    lastType = 3;
                    return "R";
                }
                break;
            case 3:
                if ((value < flatLine) && (value < values.get(nextIndex).getY())) {
                    lastType = 4;
                    return "S";
                }
                break;
            case 4:
                if ((value > flatLine) && (value > values.get(nextIndex).getY())) {
                    lastType = 0;
                    return "T";
                }
                break;
        }
        return "";
    }
}
